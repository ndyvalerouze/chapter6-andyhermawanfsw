const mongoose = require('mongoose')
const uri = 'mongosh "mongodb+srv://cluster0.3xuz8id.mongodb.net/myFirstDatabase" --apiVersion 1 --username valerouze'
mongoose.connect(uri, {
  useNewUrlParser: true, 
  useUnifiedTopology: true,
  useCreateIndex: true
})
.then(() => console.log('MongoDB connected...'))
.catch(err => console.log(err));
